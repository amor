# Irish translation of amor
# Copyright (C) 2009 This_file_is_part_of_KDE
# This file is distributed under the same license as the amor package.
# Kevin Scannell <kscanne@gmail.com>, 2009.
msgid ""
msgstr ""
"Project-Id-Version: kdetoys/amor.po\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-05 12:25+0000\n"
"PO-Revision-Date: 2004-12-03 14:52-0500\n"
"Last-Translator: Kevin Scannell <kscanne@gmail.com>\n"
"Language-Team: Irish <gaeilge-gnulinux@lists.sourceforge.net>\n"
"Language: ga\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=5; plural=n==1 ? 0 : n==2 ? 1 : n<7 ? 2 : n < 11 ? "
"3 : 4\n"

#. i18n: ectx: @info:tipoftheday
#: data/tips-en:1
#, kde-format
msgid "Don't run with scissors."
msgstr "Ná rith le siosúr."

#. i18n: ectx: @info:tipoftheday
#: data/tips-en:3
#, kde-format
msgid "Never trust car salesmen or politicians."
msgstr "Ná cuir muinín i ndíoltóir carranna nó i bpolaiteoir."

#. i18n: ectx: @info:tipoftheday
#: data/tips-en:5
#, kde-format
msgid ""
"Real programmers don't comment their code. It was hard to write, it should "
"be hard to understand."
msgstr ""
"Ní chuireann fíor-ríomhchláraitheoirí nótaí tráchta ina gcuid cóid. Bhí sé "
"deacair go leor a scríobh, ba chóir dó a bheith deacair a thuiscint."

#. i18n: ectx: @info:tipoftheday
#: data/tips-en:7
#, kde-format
msgid ""
"It is much easier to suggest solutions when you know nothing about the "
"problem."
msgstr ""
"Tá sé i bhfad níos fusa réitigh a mholadh nuair nach bhfuil eolas ar bith "
"agat ar an bhfadhb."

#. i18n: ectx: @info:tipoftheday
#: data/tips-en:9
#, kde-format
msgid "You can never have too much memory or disk space."
msgstr "Ní féidir an iomarca cuimhne nó spás diosca a bheith agat."

#. i18n: ectx: @info:tipoftheday
#: data/tips-en:11
#, kde-format
msgid "The answer is 42."
msgstr "Is é 42 an freagra."

#. i18n: ectx: @info:tipoftheday
#: data/tips-en:13
#, kde-format
msgid "It's not a bug. It's a misfeature."
msgstr "Ní fabht é. Is míghné é."

#. i18n: ectx: @info:tipoftheday
#: data/tips-en:15
#, kde-format
msgid "Help stamp out and abolish redundancy."
msgstr "Cuidigh linn deireadh a chur leis an iomarcaíocht agus é a dhíothú."

#. i18n: ectx: @info:tipoftheday
#: data/tips-en:17
#, kde-format
msgid ""
"To maximize a window vertically, click the maximize button with the middle "
"mouse button."
msgstr ""
"Chun fuinneog a uasmhéadú go hingearach, cliceáil an cnaipe 'Uasmhéadaigh' "
"leis an gcnaipe luiche sa lár."

#. i18n: ectx: @info:tipoftheday
#: data/tips-en:19
#, kde-format
msgid "You can use Alt+Tab to switch between applications."
msgstr "Is féidir leat Alt+Tab a úsáid chun bogadh idir feidhmchláir."

#. i18n: ectx: @info:tipoftheday
#: data/tips-en:21
#, kde-format
msgid ""
"Press Ctrl+Esc to show the applications running in your current session."
msgstr ""
"Brúigh Ctrl+Esc chun na feidhmchláir atá ag rith i do sheisiún reatha a "
"thaispeáint."

#. i18n: ectx: @info:tipoftheday
#: data/tips-en:23
#, kde-format
msgid "Alt+F2 displays a small window that you can type a command into."
msgstr ""
"Taispeánann Alt+F2 fuinneog bheag ar féidir leat ordú a clóscríobh inti."

#. i18n: ectx: @info:tipoftheday
#: data/tips-en:25
#, kde-format
msgid "Ctrl+F1 to Ctrl+F8 can be used to switch virtual desktops."
msgstr ""
"Is féidir leat Ctrl+F1 trí Ctrl+F8 a úsáid chun bogadh idir deasca fíorúla."

#. i18n: ectx: @info:tipoftheday
#: data/tips-en:27
#, kde-format
msgid "You can move buttons on the panel using the middle mouse button."
msgstr ""
"Is féidir leat cnaipí a bhogadh ar an bpainéal leis an gcnaipe luiche sa lár."

#. i18n: ectx: @info:tipoftheday
#: data/tips-en:29
#, kde-format
msgid "Alt+F1 pops-up the system menu."
msgstr "Taispeánann Alt+F1 roghchlár an chórais."

#. i18n: ectx: @info:tipoftheday
#: data/tips-en:31
#, kde-format
msgid ""
"Ctrl+Alt+Esc can be used to kill an application that has stopped responding."
msgstr ""
"Is féidir Ctrl+Alt+Esc a úsáid chun feidhmchlár a mharú nuair nach bhfuil sé "
"ag freagairt."

#. i18n: ectx: @info:tipoftheday
#: data/tips-en:33
#, kde-format
msgid ""
"If you leave KDE applications open when you logout, they will be restarted "
"automatically when you log back in."
msgstr ""
"Má fhágann tú feidhmchlár KDE oscailte nuair a logálann tú amach, atosófar é "
"go huathoibríoch arís nuair a logálfaidh tú isteach an chéad uair eile."

#. i18n: ectx: @info:tipoftheday
#: data/tips-en:35
#, fuzzy, kde-format
#| msgid "The KDE file manager is also a web browser and an FTP client."
msgid "The KDE file manager (Dolphin) is also an FTP client."
msgstr ""
"Is brabhsálaí Gréasáin agus cliant FTP é freisin an bainisteoir comhad KDE."

#. i18n: ectx: @info:tipoftheday
#: data/tips-en:37
#, fuzzy, kde-format
#| msgid ""
#| "Applications can display messages and tips in an Amor bubble using the "
#| "showMessage() and\n"
#| "showTip() DCOP calls"
msgid ""
"Applications can display messages and tips in an Amor bubble using the "
"showMessage() and\n"
"showTip() D-Bus calls."
msgstr ""
"Is féidir le feidhmchláir teachtaireachtaí agus leideanna a thaispeáint i "
"mbolgán Amor le\n"
"glaonna DCOP showMessage() agus showTip()"

#: src/amor.cpp:256
#, fuzzy, kde-format
#| msgctxt "@info:status"
#| msgid "Error reading theme: "
msgctxt "@info:status"
msgid "Error reading theme: %1"
msgstr "Earráid agus téama á léamh: "

#: src/amor.cpp:266 src/amor.cpp:273
#, fuzzy, kde-format
#| msgctxt "@info:status"
#| msgid "Error reading group: "
msgctxt "@info:status"
msgid "Error reading group: %1"
msgstr "Earráid agus grúpa á léamh: "

#: src/amor.cpp:519
#, kde-format
msgctxt "@action:inmenu Amor"
msgid "&Help"
msgstr "&Cabhair"

#. i18n'ed
#: src/amor.cpp:523
#, kde-format
msgctxt "@action:inmenu Amor"
msgid "&Configure..."
msgstr "&Cumraigh..."

#: src/amor.cpp:526
#, kde-format
msgctxt "@action:inmenu Amor"
msgid "&Quit"
msgstr "&Scoir"

#: src/amordialog.cpp:42
#, fuzzy, kde-format
#| msgid "Options"
msgctxt "@title:window"
msgid "Options"
msgstr "Roghanna"

#: src/amordialog.cpp:65
#, kde-format
msgid "Theme:"
msgstr "Téama:"

#: src/amordialog.cpp:82
#, kde-format
msgid "Offset:"
msgstr "Fritháireamh:"

#: src/amordialog.cpp:93
#, kde-format
msgid "Always on top"
msgstr "Chun tosaigh i gcónaí"

#: src/amordialog.cpp:98
#, kde-format
msgid "Show random tips"
msgstr "Taispeáin leideanna randamacha"

#: src/amordialog.cpp:103
#, kde-format
msgid "Use a random character"
msgstr "Úsáid carachtar randamach"

#: src/amordialog.cpp:108
#, kde-format
msgid "Allow application tips"
msgstr "Ceadaigh leideanna fheidhmchláir"

#: src/main.cpp:28
#, kde-format
msgid "KDE creature for your desktop"
msgstr "Créatúr KDE ar do dheasc"

#: src/main.cpp:38
#, kde-format
msgid "amor"
msgstr "amor"

#: src/main.cpp:41
#, kde-format
msgid ""
"1999 by Martin R. Jones\n"
"2010 by Stefan Böhmann"
msgstr ""
"1999 Martin R. Jones\n"
"2010 Stefan Böhmann"

#: src/main.cpp:44
#, kde-format
msgid "Stefan Böhmann"
msgstr "Stefan Böhmann"

#: src/main.cpp:45
#, kde-format
msgid "Current maintainer"
msgstr "Cothaitheoir reatha"

#: src/main.cpp:51
#, kde-format
msgid "Martin R. Jones"
msgstr "Martin R. Jones"

#: src/main.cpp:52
#, kde-format
msgid "Gerardo Puga"
msgstr "Gerardo Puga"

#~ msgid "No tip"
#~ msgstr "Gan leid"

#~ msgid "Copyright 1999, Martin R. Jones"
#~ msgstr "Cóipcheart 1999, Martin R. Jones"

#~ msgctxt "@label:textbox"
#~ msgid ""
#~ "Amor Version %1\n"
#~ "\n"
#~ msgstr ""
#~ "Amor Leagan %1\n"
#~ "\n"

#~ msgctxt "@label:textbox"
#~ msgid ""
#~ "Amusing Misuse Of Resources\n"
#~ "\n"
#~ msgstr ""
#~ "Mí-úsáid Ghreannmhar na hAcmhainní\n"
#~ "\n"

#~ msgctxt "@label:textbox"
#~ msgid ""
#~ "Copyright 1999 Martin R. Jones <email>mjones@kde.org</email>\n"
#~ "\n"
#~ msgstr ""
#~ "Cóipcheart © 1999 Martin R. Jones <email>mjones@kde.org</email>\n"
#~ "\n"

#~ msgctxt "@label:textbox"
#~ msgid "Original Author: Martin R. Jones <email>mjones@kde.org</email>\n"
#~ msgstr "An chéad údar: Martin R. Jones <email>mjones@kde.org</email>\n"

#~ msgctxt "@label:textbox"
#~ msgid ""
#~ "Current Maintainer: Gerardo Puga <email>gpuga@gioia.ing.unlp.edu.ar</"
#~ "email>\n"
#~ msgstr ""
#~ "Cothaitheoir reatha: Gerardo Puga <email>gpuga@gioia.ing.unlp.edu.ar</"
#~ "email>\n"

#~ msgctxt "@label:textbox"
#~ msgid "About Amor"
#~ msgstr "Eolas Faoi Amor"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Séamus Ó Ciardhuáin,Kevin Scannell"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "seoc@iolfree.ie,kscanne@gmail.com"
